
from flask import Flask, jsonify, request
import mysql.connector
from hashlib import sha1
import subprocess
import requests
app = Flask(__name__)

AGE = "/run/current-system/sw/bin/age"

dbHost = "database"
dbName = "imovies"
dbUser = "backend"
dbPasword = open("/var/lib/backend_app/database_password").read().strip()
dbSslCert = "/var/lib/backend_app/database_server.pem"

# optional : add session management (list of connected and logout)


def encryptSk(secretKey):
    proc = subprocess.run([AGE, "-a", "-R", "/etc/imovies/recovery-keys.key"], input=secretKey, capture_output=True, check=True)
    return proc.stdout

def askCAGen(name, email):
    # TODO TLS
    rep = requests.post("http://ca-generator/personal_certificate/create", json={'name': name, 'email': email})
    return rep.json()['private_key'], rep.json()['certificate']
    
# ##########################################
# # 
@app.route("/login", methods = ["POST"]) 
def login():
    args = request.get_json(force = True)

    email = args["email"]
    pswd = args["password"]

    h = sha1()
    h.update(pswd.encode())
    shap = h.hexdigest()

    with mysql.connector.connect(host = dbHost, database = dbName, username = dbUser, password = dbPasword, ssl_ca=dbSslCert, ssl_verify_cert=True) as connection:
        with connection.cursor(prepared = True) as cursor:


            loginq = """SELECT uid, lastname, firstname, email FROM users WHERE email = %s AND pwd = %s"""
            cursor.execute(loginq, (email, shap))

            tmp = cursor.fetchall()
            
            if cursor.rowcount <= 0 :
                return None

            info = tmp[0]
            cursor.reset()
    
    return {"uid" : info[0], "lastname" : info[1], "firstname" : info[2], "email" : info[3]}


# ##########################################
# #
@app.route("/cert", methods = ["POST"]) 
def logincert():
    cert = request.get_json(force = True)
    

    with mysql.connector.connect(host = dbHost, database = dbName, username = dbUser, password = dbPasword) as connection :
        with connection.cursor(prepared = True) as cursor :
                    
            revq = """SELECT * FROM CRL WHERE certificate = %s"""
            cursor.execute(revq, (cert,))
            tmp = cursor.fetchall()

            if cursor.rowcount > 0 :
                return None

            cursor.reset()
            issq = """SELECT user_uid, is_admin FROM ca_certificates WHERE certificate = %s"""
            cursor.execute(issq, (cert,)) 
            tmp = cursor.fetchall()

            if cursor.rowcount <= 0 :
                return None

            uid, isadmin = tmp[0]

            cursor.reset()
            loginq = """SELECT uid, lastname, firstname, email FROM users WHERE uid = %s"""
            cursor.execute(loginq, (uid,))
            tmp = cursor.fetchall()

            if cursor.rowcount <= 0 :
                return None

            info = tmp[0]


    return {"uid" : info[0], "lastname" : info[1], "firstname" : info[2], "email" : info[3], "isadmin" : str(isadmin)}

# ##########################################
# # 
@app.post("/update") 
def changeInfo():
    args = request.get_json(force = True)

    ln = args["last_name"]
    fn = args["first_name"]
    email = args["email"]
    uid = args["uid"]

    with mysql.connector.connect(host = dbHost, database = dbName, username = dbUser, password = dbPasword) as connection :
        with connection.cursor(prepared = True) as cursor :
                    
            changeq = """UPDATE users SET lastname = %s, firstname = %s, email = %s WHERE uid = %s"""
            cursor.execute(changeq, (ln, fn, email, uid))
            connection.commit()

    return {"Success" : "True"}


# #########################################
# # 
@app.post("/gen") 
def genCert():
    args = request.get_json(force = True)

    ln = args["last_name"]
    fn = args["first_name"]
    email = args["email"]
    uid = args["uid"]

    sk, cert = askCAGen(f"{ln} {fn}", email)
    esk = encryptSk(sk.encode())

    
    with mysql.connector.connect(host = dbHost, database = dbName, username = dbUser, password = dbPasword) as connection :
        with connection.cursor(prepared = True) as cursor :

            addq = """INSERT INTO ca_certificates (certificate, enc_private_key) VALUES (%s, %s)"""
            cursor.execute(addq, (cert, esk),)
            connection.commit()

    return {"secretKey" :sk, "certificate" : cert}

# ##########################################
# # 
@app.post("/revoke") 
def revokeCert():
    cert = request.get_json(force = True)

    sig = requests.post("http://ca-generator/personal_certificate/revoke", {"certificate" : cert}).json()

    with mysql.connector.connect(host = dbHost, database = dbName, username = dbUser, password = dbPasword) as connection :
        with connection.cursor(prepared = True) as cursor :
                    
            revq = """INSERT INTO CRL (certificate, signature) VALUES (%s, %s)"""

            cursor.execute(revq, (cert, sig))
            connection.commit()

    return {"Success" : "True"}


# ##########################################
# # 
@app.get("/admin") 
def admin_info():

    serial = requests.get("http://ca-generator/personal_certificate/index").json()

    
    with  mysql.connector.connect(host = dbHost, database = dbName, username = dbUser, password = dbPasword) as connection:
        with connection.cursor(prepared = True) as cursor:

            issuedq = """SELECT * FROM ca_certificates"""
            cursor.execute(issuedq)
            tmp = cursor.fetchall()
            nbiss = cursor.rowcount
            cursor.reset()

            revq = """SELECT * FROM CRL"""
            cursor.execute(revq)
            tmp = cursor.fetchall()
            nbrev = cursor.rowcount


    return {"issuedNumber" : str(nbiss), "revokedNumber" : str(nbrev), "serial" : serial}
