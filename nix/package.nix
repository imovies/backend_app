{python3Packages}:
python3Packages.buildPythonPackage {
  pname = "backend-app";
  version = "0.1";

  src = builtins.path {
    path = ../.;
    name = "source";
  };

  propagatedBuildInputs = with python3Packages; [flask requests mysql-connector];

  format = "pyproject";
  nativeBuildInputs = [python3Packages.setuptools];
}
